# V92-C-Base
### My base character content

Base files used in a large number of my character models and such.

# A game-ready version can be downloaded via Workshop

## Faces/Eyes/Arms - Mostly the important stuff.
### https://steamcommunity.com/workshop/filedetails/?id=851168781
## Body & Mesh textures - used in complete overhauls and such.
### https://steamcommunity.com/workshop/filedetails/?id=1171821076
